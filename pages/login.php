<div class="container">
<div class="center">

<form class="form-inline form-login" method="post" action="<?php echo LOGIN; ?>">
  <div class="form-group">
    <label class="sr-only" for="username">Username</label>
    <input name="username" type="username" class="form-control" id="username" placeholder="username">
  </div>
  <div class="form-group">
    <label class="sr-only" for="password">Password</label>
    <input name="password" type="password" class="form-control" id="password" placeholder="Password">
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-default">Sign in</button>
  </div>
</form>

</div>
</div>

