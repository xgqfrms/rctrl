<?php

namespace lib;

use lib\Disks;

$disks = Disks::disks();

function label_partition($status) {
  echo '<span class="label label-';
  switch ($status) {
    default:
      echo 'success';
      break;
    case '':
      echo 'danger';
      break;
  }
  echo '">';
  switch ($status) {
    default:
      echo 'Mounted';
      break;
    case '':
      echo 'Not Mounted';
      break;
  }
  echo '</span>';
}
?>

<div class="container details">
  <?php
    for ($i = 0; $i < sizeof($disks); $i++) {
      echo '<div class="panel panel-default">';

      if ($disks[$i]["type"] != "disk") {
        echo '<div class="panel-body">';
          if (strpos($disks[$i]['name'], "sda") !== false) {
            echo '<a data-rootaction="changepartitionstatus" data-partition-name="' . $disks[$i]["name"] . '" data-curr-mountpoint="' . $disks[$i]["mountpoint"] . '" class="rootaction" href="javascript:;">';
            echo label_partition($disks[$i]['mountpoint']), '</a>';
          }
          else
            echo label_partition($disks[$i]['mountpoint']), '';
          echo '<br><i>', $disks[$i]['name'] . "</i><br>Size: " . $disks[$i]['size'] . "<br>Mountpoint: " . $disks[$i]['mountpoint'], '';
        echo '</div>';
      }
      else
        echo '<div class="panel-heading">', $disks[$i]['name'], '</div>';

      echo '</div>';
    }
  ?>
</div>
