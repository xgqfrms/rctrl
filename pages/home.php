<?php

namespace lib;

use lib\Uptime;
use lib\Memory;
use lib\CPU;
use lib\Storage;
use lib\Network;
use lib\Rbpi;
use lib\Users;
use lib\Temp;

$uptime = Uptime::uptime();
$ram = Memory::ram();
$swap = Memory::swap();
$cpu = CPU::cpu();
$cpu_heat = CPU::heat();
$hdd = Storage::hdd();
$hdd_alert = 'success';
for ($i = 0; $i < sizeof($hdd); $i++) {
    if ($hdd[$i]['alert'] == 'warning')
        $hdd_alert = 'warning';
}
$network = Network::connections();
$users = sizeof(Users::connected());
$temp = Temp::temp();

$external_ip = Rbpi::externalIp();

function icon_alert($alert) {
  echo '<i class="glyphicon glyphicon-';
  switch ($alert) {
    case 'success':
      echo 'ok';
      break;
    case 'warning':
      echo 'warning-sign';
      break;
    default:
      echo 'exclamation-sign';
  }
  echo ' pull-right"></i>';
}
?>

<div class="container home">
  <div class="row infos">
    <div class="col-sm-4">
      <i class="glyphicon glyphicon-home"></i> <?php echo Rbpi::hostname(); ?>
    </div>
    <div class="col-sm-4">
      <i class="glyphicon glyphicon-map-marker"></i> <?php echo Rbpi::internalIp(); ?>
      <?php echo ($external_ip != 'Unavailable') ? '<br /><i class="glyphicon glyphicon-globe"></i> ' . $external_ip : ''; ?>
    </div>
    <div class="col-sm-4">
      <i class="glyphicon glyphicon-play-circle"></i> Server <?php echo Rbpi::webServer(); ?>
    </div>
  </div>

  <div class="row infos">
    <div class="col-sm-12">
      <a href="<?php echo DETAILS; ?>#check-uptime"><i class="glyphicon glyphicon-time"></i></a> <?php echo $uptime; ?>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <div>
          <i class="glyphicon glyphicon-asterisk"></i> RAM <a href="<?php echo DETAILS; ?>#check-ram"><?php echo icon_alert($ram['alert']); ?></a>
      </div>
      <div>
          <i class="glyphicon glyphicon-refresh"></i> Swap <a href="<?php echo DETAILS; ?>#check-swap"><?php echo icon_alert($swap['alert']); ?></a>
      </div>
      <div>
          <i class="glyphicon glyphicon-tasks"></i> CPU <a href="<?php echo DETAILS; ?>#check-cpu"><?php echo icon_alert($cpu['alert']); ?></a>
      </div>
      <div>
          <i class="glyphicon glyphicon-fire"></i> CPU <a href="<?php echo DETAILS; ?>#check-cpu-heat"><?php echo icon_alert($cpu_heat['alert']); ?></a>
      </div>
    </div>
    <div class="col-sm-6">
      <div>
          <i class="glyphicon glyphicon-hdd"></i> Storage <a href="<?php echo DETAILS; ?>#check-storage"><?php echo icon_alert($hdd_alert); ?></a>
      </div>
      <div>
          <i class="glyphicon glyphicon-globe"></i> Network <a href="<?php echo DETAILS; ?>#check-network"><?php echo icon_alert($network['alert']); ?></a>
      </div>
      <div>
          <i class="glyphicon glyphicon-user"></i> Users <a href="<?php echo DETAILS; ?>#check-users"><span class="badge pull-right"><?php echo $users; ?></span></a>
      </div>
      <div>
          <i class="glyphicon glyphicon-fire"></i> Temperature <a href="<?php echo DETAILS; ?>#check-temp"><?php echo icon_alert($temp['alert']); ?></a>
      </div>
    </div>
  </div>
</div>
