# Raspcontrol - revamped #

Raspcontrol is a web control centre written in PHP for Raspberry Pi.

This is a Fork of the orignal Raspcontrol by Bioshox - It appears the repo is no longer available...

***

## Screenshots ##

[imgur](https://imgur.com/a/XlcTHEG)

![screen](https://i.imgur.com/QP3kLfa.png)

## Installation

You need a web server installed on your Raspberry Pi.


If you are in a hurry, just clone the repository with:

	git clone https://gitlab.com/blackout314/rctrl.git

And create the json authentifation file `/etc/raspcontrol/database.aptmnt` with 740 rights and owned by www-data:

	{
 	   "user":       "yourName",
 	   "password":   "yourPassword"
	}

## Optional configuration

In order to have some beautiful URLs, you can enable URL Rewriting.  
__Note:__ It's not necessary to enable URL Rewriting to use Raspcontrol.

## TODO ##
 * [x] bootstrap 3
 * [x] gauge
 * [ ] clean code
 * [ ] fix gpio